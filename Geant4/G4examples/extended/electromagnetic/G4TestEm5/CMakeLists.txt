gaudi_depends_on_subdirs(Geant4/G4config)

gaudi_install_scripts()

gaudi_alias(MultipleScattering_test  Em5run.sh)

# CMT-compatibility alias
gaudi_alias(testEm5.exe TestEm5)

# Required for tests
find_package(ROOT)
